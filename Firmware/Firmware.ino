#include <EEPROM.h>
#include "Keyboard.h"

#define EEPROM_VALIDATION_FLAG 0x03
#define SEND_CONFIG_FLAG 'S'
#define RECEIVE_CONFIG_FLAG 'R'
#define NUMBER_OF_BUTTONS 16
#define BUTTON_TEXT_LENGTH 32
#define BUTTON_CONFIG_LENGTH 36
#define DEBUG false
#define DEBOUNCE 50

const uint8_t pinout[NUMBER_OF_BUTTONS] = {2, 3, 4, 5, 6, 7, 8, 9, 10, 16, 14, 15, A0, A1, A2, A3};
uint32_t keyDownTime[NUMBER_OF_BUTTONS];
uint32_t repeatPressTime[NUMBER_OF_BUTTONS];
bool prevButtonState[NUMBER_OF_BUTTONS];
bool firstTimePressed[NUMBER_OF_BUTTONS];

struct Button {
  char text[BUTTON_TEXT_LENGTH] = {0};
  bool is_text=0, mod_ctrl=0, mod_shift=0, mod_alt=0;
  // is_text that is set to true means that key prints plain text instead of clicking combination.
  // Combination symbol in this case saved in first position of text[] array
};

Button* buttons;

void saveConfig(Button* config) {
  EEPROM.update(0, EEPROM_VALIDATION_FLAG);
  for (int i = 1; i <= sizeof(Button) * NUMBER_OF_BUTTONS; i++)
    EEPROM.update(i, *((char*)config + i - 1));
}

Button* loadConfig() {
  Button* config = new Button[NUMBER_OF_BUTTONS];
  if (EEPROM.read(0) == EEPROM_VALIDATION_FLAG) {
    for (int i = 1; i <= sizeof(Button) * NUMBER_OF_BUTTONS; i++)
      *((char*)config + i - 1) = EEPROM.read(i);
  }
  return config;
}

void sendConfig() {
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    Button* button = &buttons[i];
    Serial.write(button->is_text);
    Serial.write(button->mod_ctrl);
    Serial.write(button->mod_shift);
    Serial.write(button->mod_alt);
    for (int j = 0; j < BUTTON_TEXT_LENGTH; j++) {
      Serial.write(button->text[j]);
    }
  }
  Serial.println();
}

void receiveConfig() {
  String config = Serial.readStringUntil('\r');
  if (config.length() != BUTTON_CONFIG_LENGTH * NUMBER_OF_BUTTONS) {
    return;
  }
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    Button* button = &buttons[i];
    button->is_text = config[BUTTON_CONFIG_LENGTH*i];
    button->mod_ctrl = config[BUTTON_CONFIG_LENGTH*i + 1];
    button->mod_shift = config[BUTTON_CONFIG_LENGTH*i + 2];
    button->mod_alt = config[BUTTON_CONFIG_LENGTH*i + 3];
    for (int j = 0; j < BUTTON_TEXT_LENGTH; j++) {
      button->text[j] = config[BUTTON_CONFIG_LENGTH*i + 4 + j];
    }
  }
  saveConfig(buttons);
  Serial.println("OK");
}

int freeRam() {
  extern int __heap_start, *__brkval; 
  int v; 
  return (int) &v - (__brkval == 0 ? (int) &__heap_start : (int) __brkval); 
}

void setup() {
  // Configure pins
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    pinMode(pinout[i], INPUT_PULLUP);
  }

  // Initiate Serial connection
  Serial.begin(115200);
  Serial.setTimeout(100);
  while (!Serial) ;

  // Load buttons configuration from memory
  buttons = loadConfig();

  // If validation flags mismatching - write new config to memory
  if (EEPROM.read(0) != EEPROM_VALIDATION_FLAG) {
    if (DEBUG) {
      Serial.println("Validation flag mismatching ('" + String(EEPROM.read(0)) + "' instead of '" + String(EEPROM_VALIDATION_FLAG) + "'), memory will be cleared");
    }
    saveConfig(buttons);
  }

  // Show debug info if debug mode enabled
  if (DEBUG) {
    Serial.println(String(F("Size of whole button config: ")) + (sizeof(Button) * NUMBER_OF_BUTTONS) + F("B from 1024B available"));
    Serial.println(String(F("RAM available: ")) + freeRam() + "B");

    Serial.println(F("Button configuration: "));
    for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
      Button* button = &buttons[i];

      Serial.print(String(i+1) + ":\tCTRL:" + button->mod_ctrl + ", SHILT:" + button->mod_shift + ", ALT:" + button->mod_alt + (button->is_text ? ", Text: " : ", Key: "));
      for (int j = 0; j < BUTTON_TEXT_LENGTH && button->text[j]; j++) {
        Serial.print(button->text[j]);
      }
      Serial.println();
    }
  }
  
  Keyboard.begin();

  // Prepare working variables
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    prevButtonState[i] = LOW;
    firstTimePressed[i] = false;
  }
}

void pressKey(uint8_t key) {
  Button* button = &buttons[key];

  if (button->is_text) {
    // Type text
    for (int i = 0; i < BUTTON_TEXT_LENGTH && button->text[i]; i++) {
      Keyboard.press(button->text[i]);
      Keyboard.release(button->text[i]);
    }
  } else {
    // Click combination
    if (button->mod_ctrl) Keyboard.press(KEY_LEFT_CTRL);
    if (button->mod_shift) Keyboard.press(KEY_LEFT_SHIFT);
    if (button->mod_alt) Keyboard.press(KEY_LEFT_ALT);

    // Key for combination stored in first symbol of text string
    Keyboard.press(button->text[0]);
    Keyboard.release(button->text[0]);
  }

  Keyboard.releaseAll();
}

void loop() {
  for (int i = 0; i < NUMBER_OF_BUTTONS; i++) {
    bool buttonState = !digitalRead(pinout[i]);

    // When keydown detected - remember current time
    if (buttonState == HIGH && prevButtonState[i] == LOW) {
      keyDownTime[i] = millis();
      repeatPressTime[i] = 0;
    }

    // When keyup detected - check if this press valid and if true - press corresponding key
    if (buttonState == HIGH && (millis() - keyDownTime[i] > DEBOUNCE) && !firstTimePressed[i]) {
      pressKey(i);
      firstTimePressed[i] = true;
      if (DEBUG) {
        Serial.println(String("Key pressed: ") + (i+1));
      }
    }
   if (buttonState == HIGH && (millis() - keyDownTime[i] > 500) && (millis() - repeatPressTime[i] > 30)) {
    pressKey(i);
    repeatPressTime[i] = millis();
    if (DEBUG) {
      Serial.println(String("Key hold: ") + (i+1));
    }
   }

    if (buttonState == LOW) {
      firstTimePressed[i] = false;
    }

    prevButtonState[i] = buttonState;
  }

  // Process input commands
  while (Serial.available()) {
    char flag = Serial.read();
    switch (flag){
      case SEND_CONFIG_FLAG:
        sendConfig();
        break;
      case RECEIVE_CONFIG_FLAG:
        receiveConfig();
        break;
    }
  }
}
