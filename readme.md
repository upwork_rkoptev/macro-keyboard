# Macro keyboard

This repository store firmware and configuration program for mini keyboard based on Arduino Micro. You can find executables on Windows at following path:
	./Configurator/dist

## Building
(This instruction more useful for developers)

To rebuild this project you need the following software installed on your PC:

- Python 3.4.4 (32 bit - limitation of py2exe)
- PyQt4-4.11.4-gpl-Py3.4-Qt4.8.7-x32
- pyserial-3.2.1
- py2exe-0.9.2.2

This versions are not strictly defined, but I can guarantee proper work on this versions.

Also, to flash the keyboard firmware you need to install Arduino IDE (tested on 1.6.11 version)

If all this dependencies was installed, next step is to clone this repository to your PC.

	$ git clone https://rkoptev@bitbucket.org/upwork_rkoptev/macro-keyboard.git

Next, navigate to "Configurator" folder and run this command to build the project:

	$ python setup.py py2exe

If all goes well - you will see a new folder "dist". This folder contains builded project for Windows. You can run program by double-clicking on main.exe
![picture](screenshot.jpg)

## Changing VID/PID of device
To change VID/PID open "Configurator/keyboard.py" file in text editor. On 17th line you'll see following code:
	
	if "VID:PID=2341:8037" in port[2] or "VID:PID=2341:8036" in port[2]:

This ids for Arduino Micro and Arduino Leonardo respectively.
You can remove unnecessary ids or add own:
	
	if "VID:PID=2341:8037" in port[2] or "VID:PID=2341:8036" in port[2] or "VID:PID=*your_VID*:*your_PID*" in port[2]:

## Wiring diagram
![picture](Wiring_diagram.jpg)