from PyQt4 import QtGui, QtCore

import KeyDictionary


class QKeyCombinationEdit(QtGui.QLineEdit):
    def __init__(self, parent=None):
        QtGui.QLineEdit.__init__(self, parent)

        self.ctrl = 0
        self.shift = 0
        self.alt = 0
        self.key = 0

    # (ctrl, shift, alt)
    @staticmethod
    def parse_modifiers(modifiers):
        if modifiers == QtCore.Qt.ControlModifier:
            return True, False, False
        elif modifiers == QtCore.Qt.ShiftModifier:
            return False, True, False
        elif modifiers == QtCore.Qt.AltModifier:
            return False, False, True
        elif modifiers == QtCore.Qt.ControlModifier | QtCore.Qt.ShiftModifier:
            return True, True, False
        elif modifiers == QtCore.Qt.ShiftModifier | QtCore.Qt.AltModifier:
            return False, True, True
        elif modifiers == QtCore.Qt.AltModifier | QtCore.Qt.ControlModifier:
            return True, False, True
        elif modifiers == QtCore.Qt.ControlModifier | QtCore.Qt.ShiftModifier | QtCore.Qt.AltModifier:
            return True, True, True
        else:
            return False, False, False

    def event(self, QEvent):
        if QEvent.type() == QtCore.QEvent.KeyPress and QEvent.key() != QtCore.Qt.Key_Control and QEvent.key() != QtCore.Qt.Key_Shift and QEvent.key() != QtCore.Qt.Key_Alt:
            scan_code = QEvent.nativeScanCode()
            ctrl, shift, alt = self.parse_modifiers(QEvent.modifiers())

            # print("Scan code: " + str(scan_code))
            # print("Key: " + KeyDictionary.scan_code_to_text(scan_code))

            # Skip unrecognized keys
            if not KeyDictionary.is_valid_scan_code(scan_code):
                return True

            self.set_combination(KeyDictionary.scan_code_to_key_code(scan_code), ctrl, shift, alt)

            return True
        else:
            return QtGui.QLineEdit.event(self, QEvent)

    def keyReleaseEvent(self, QKeyEvent):
        pass

    # Disable context menu
    def contextMenuEvent(self, QContextMenuEvent):
        pass

    def set_combination(self, key, ctrl, shift, alt):
        # Skip unrecognized keys
        if not KeyDictionary.is_valid_key_code(key):
            return False

        # Save key in object
        self.key = key
        self.ctrl = ctrl
        self.shift = shift
        self.alt = alt

        # And display it
        text = ""
        if ctrl:
            text += "Ctrl + "
        if shift:
            text += "Shift + "
        if alt:
            text += "Alt + "
        text += KeyDictionary.key_code_to_text(key)
        self.setText(text)
        return True

    def combination(self):
        return self.key, self.ctrl, self.shift, self.alt

    def clear_combination(self):
        self.key = 0
        self.ctrl = 0
        self.shift = 0
        self.alt = 0

        self.setText("")
