# Format: (scancode, arduino_key_code, text_representation)

keys = (
    # Numbers
    (2, 0x31, "1"),
    (3, 0x32, "2"),
    (4, 0x33, "3"),
    (5, 0x34, "4"),
    (6, 0x35, "5"),
    (7, 0x36, "6"),
    (8, 0x37, "7"),
    (9, 0x38, "8"),
    (10, 0x39, "9"),
    (11, 0x30, "0"),

    # Letters
    (30, 0x61, "A"),
    (48, 0x62, "B"),
    (46, 0x63, "C"),
    (32, 0x64, "D"),
    (18, 0x65, "E"),
    (33, 0x66, "F"),
    (34, 0x67, "G"),
    (35, 0x68, "H"),
    (23, 0x69, "I"),
    (36, 0x6a, "J"),
    (37, 0x6b, "K"),
    (38, 0x6c, "L"),
    (50, 0x6d, "M"),
    (49, 0x6e, "N"),
    (24, 0x6f, "O"),
    (25, 0x70, "P"),
    (16, 0x71, "Q"),
    (19, 0x72, "R"),
    (31, 0x73, "S"),
    (20, 0x74, "T"),
    (22, 0x75, "U"),
    (47, 0x76, "V"),
    (17, 0x77, "W"),
    (45, 0x78, "X"),
    (21, 0x79, "Y"),
    (44, 0x7a, "Z"),

    # Spec symbols
    (57, 0x20, "Space"),
    (12, 0x2d, "-"),
    (13, 0x3d, "="),
    (26, 0x5b, "["),
    (27, 0x5d, "]"),
    (43, 0x5c, "\\"),
    (39, 0x3b, ";"),
    (40, 0x27, "'"),
    (51, 0x2c, ","),
    (52, 0x2e, "."),
    (53, 0x2f, "/"),
    (41, 0x60, "`"),

    # Functional keys
    (59, 0xc2, "F1"),
    (60, 0xc3, "F2"),
    (61, 0xc4, "F3"),
    (62, 0xc5, "F4"),
    (63, 0xc6, "F5"),
    (64, 0xc7, "F6"),
    (65, 0xc8, "F7"),
    (66, 0xc9, "F8"),
    (67, 0xca, "F9"),
    (68, 0xcb, "F10"),
    (87, 0xcc, "F11"),
    (88, 0xcd, "F12"),

    # Other keys
    (328, 0xda, "Up"),
    (336, 0xd9, "Down"),
    (331, 0xd8, "Left"),
    (333, 0xd9, "Right"),
    (14, 0xb2, "Backspace"),
    (15, 0xb3, "Tab"),
    (28, 0xb0, "Enter"),
    (1, 0xb1, "Esc"),
    (338, 0xd1, "Insert"),
    (339, 0xd4, "Del"),
    (329, 0xd3, "PageUp"),
    (337, 0xd6, "PageDown"),
    (327, 0xd2, "Home"),
    (335, 0xd5, "End"),

    # Num pad
    (309, 0xdc, "Num /"),
    (55, 0xdd, "Num *"),
    (74, 0xde, "Num -"),
    (78, 0xdf, "Num +"),
    (284, 0xe0, "Num Enter"),
    (79, 0xe1, "Num 1"),
    (80, 0xe2, "Num 2"),
    (81, 0xe3, "Num 3"),
    (75, 0xe4, "Num 4"),
    (76, 0xe5, "Num 5"),
    (77, 0xe6, "Num 6"),
    (71, 0xe7, "Num 7"),
    (72, 0xe8, "Num 8"),
    (73, 0xe9, "Num 9"),
    (82, 0xea, "Num 0"),
    (83, 0xeb, "Num Del")

)


def scan_code_to_text(scan_code):
    for i in range(len(keys)):
        if keys[i][0] == scan_code:
            return keys[i][2]
    return ""


def key_code_to_text(key_code):
    for i in range(len(keys)):
        if keys[i][1] == key_code:
            return keys[i][2]
    return ""


def scan_code_to_key_code(scan_code):
    for i in range(len(keys)):
        if keys[i][0] == scan_code:
            return keys[i][1]
    return 0


def key_code_to_scan_code(key_code):
    for i in range(len(keys)):
        if keys[i][1] == key_code:
            return keys[i][0]
    return 0


def is_valid_scan_code(scan_code):
    for i in range(len(keys)):
        if keys[i][0] == scan_code:
            return True
    return False


def is_valid_key_code(key_code):
    for i in range(len(keys)):
        if keys[i][1] == key_code:
            return True
    return False
