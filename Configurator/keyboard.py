import time

import serial
import serial.tools.list_ports

SEND_CONFIG_FLAG = b'S'
RECEIVE_CONFIG_FLAG = b'R'


class Keyboard:
    port = None

    def __init__(self):
        self.connect()

    def connect(self):
        for port in list(serial.tools.list_ports.comports()):
            if "VID:PID=2341:8037" in port[2] or "VID:PID=2341:8036" in port[2]:
                self.port = serial.Serial(port[0], 115200, timeout=3)
                time.sleep(1)
                return True
        print("Can't connect")
        return False

    def load(self, try_again=True):
        try:
            # Flush serial buffer
            self.port.reset_input_buffer()
            # Send request of configuration to keyboard
            self.port.write(SEND_CONFIG_FLAG)
            # Receive response package from keyboard
            return self.port.readline()[:-2]
        # if error occurs - try to reconnect
        except (AttributeError, serial.SerialException):
            if try_again:
                if not self.connect():
                    return None
                else:
                    return self.load(try_again=False)
            else:
                return None

    def upload(self, config, try_again=True):
        try:
            # Flush serial buffer
            self.port.reset_input_buffer()
            # Send flag that means we send new config
            self.port.write(RECEIVE_CONFIG_FLAG)
            # Send config to keyboard
            self.port.write(config + b"\r")
            # Make sure that uploading goes OK
            return self.port.readline() == b'OK\r\n'
        # if error occurs - try to reconnect
        except (AttributeError, serial.SerialException):
            if try_again:
                if not self.connect():
                    return None
                else:
                    return self.upload(config, try_again=False)
            else:
                return None
