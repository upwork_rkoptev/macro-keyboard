import sys

from PyQt4 import QtGui, QtCore

import keyboard
from QKeyCombinationEdit import QKeyCombinationEdit

BUTTONS_COUNT = 16
BUTTON_TEXT_LENGTH = 32
BUTTON_CONFIG_LENGTH = BUTTON_TEXT_LENGTH + 4
STYLESHEET_FILENAME = "assets/stylesheet.qss"


class Configurator(QtGui.QWidget):
    board = None
    key_combination_edits = []
    text_fields = []
    is_text_checkboxes = []

    def __init__(self, parent=None):
        QtGui.QWidget.__init__(self, parent)

        self.draw_gui()
        self.is_text_checkboxes_state_changed()

        with open(STYLESHEET_FILENAME, "r") as file:
            self.setStyleSheet(file.read())

        self.board = keyboard.Keyboard()

    def draw_gui(self):
        self.setFixedSize(350, 500)
        self.setWindowTitle("Mini Keyboard Configurator")
        self.setObjectName("window")

        vbox = QtGui.QVBoxLayout()
        header = QtGui.QLabel("<h1>Mini Keyboard Configurator</h1>")
        header.setObjectName("header")
        vbox.addWidget(header)

        for i in range(BUTTONS_COUNT):
            hbox = QtGui.QHBoxLayout()

            # Key combination edit
            combination_edit = QKeyCombinationEdit()
            combination_edit.setAlignment(QtCore.Qt.AlignHCenter)
            combination_edit.setPlaceholderText("Press key")
            hbox.addWidget(combination_edit)
            self.key_combination_edits.append(combination_edit)

            # Text edit
            text_edit = QtGui.QLineEdit()
            text_edit.setAlignment(QtCore.Qt.AlignHCenter)
            text_edit.setMaxLength(BUTTON_TEXT_LENGTH)
            text_edit.setPlaceholderText("Enter text")
            hbox.addWidget(text_edit)
            self.text_fields.append(text_edit)

            # Is text checkboxes
            is_text_checkbox = QtGui.QCheckBox()
            is_text_checkbox.setText("Text")
            is_text_checkbox.stateChanged.connect(self.is_text_checkboxes_state_changed)
            hbox.addWidget(is_text_checkbox)
            self.is_text_checkboxes.append(is_text_checkbox)

            vbox.addLayout(hbox)

        buttons_hbox = QtGui.QHBoxLayout()

        open_btn = QtGui.QPushButton()
        open_btn.setText("Open")
        open_btn.clicked.connect(self.open)
        buttons_hbox.addWidget(open_btn)

        save_btn = QtGui.QPushButton()
        save_btn.setText("Save")
        save_btn.clicked.connect(self.save)
        buttons_hbox.addWidget(save_btn)

        load_btn = QtGui.QPushButton()
        load_btn.setText("Load")
        load_btn.clicked.connect(self.load)
        buttons_hbox.addWidget(load_btn)

        upload_btn = QtGui.QPushButton()
        upload_btn.setText("Upload")
        upload_btn.clicked.connect(self.upload)
        buttons_hbox.addWidget(upload_btn)

        vbox.addLayout(buttons_hbox)

        self.setLayout(vbox)

    @staticmethod
    def show_reconnect_dialog():
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Warning)
        msg.setText("Connect the keyboard")
        msg.setInformativeText("It seems that the keyboard is not connected. Connect the keyboard and try again.")
        msg.setStandardButtons(QtGui.QMessageBox.Retry | QtGui.QMessageBox.Close)
        return msg.exec_()

    @staticmethod
    def show_upload_success_message():
        msg = QtGui.QMessageBox()
        msg.setIcon(QtGui.QMessageBox.Information)
        msg.setText("Upload succesfull")
        return msg.exec_()

    def is_text_checkboxes_state_changed(self):
        for i in range(BUTTONS_COUNT):
            is_text = self.is_text_checkboxes[i].isChecked()
            if is_text:
                self.key_combination_edits[i].hide()
                self.key_combination_edits[i].clear_combination()
                self.text_fields[i].show()
            else:
                self.key_combination_edits[i].show()
                self.text_fields[i].hide()
                self.text_fields[i].setText("")

    def set_config(self, config):
        if (type(config) != bytes) or (len(config) != BUTTON_CONFIG_LENGTH * BUTTONS_COUNT):
            raise AttributeError("Wrong config: " + format(config))
        for i in range(BUTTONS_COUNT):
            is_text = config[BUTTON_CONFIG_LENGTH * i]

            self.is_text_checkboxes[i].setChecked(is_text)
            if is_text:
                text = config[
                       (BUTTON_CONFIG_LENGTH * i + 4):(BUTTON_CONFIG_LENGTH * i + 4 + BUTTON_TEXT_LENGTH)].decode(
                    "latin-1").strip("\0")
                self.text_fields[i].setText(text)
            else:
                ctrl = config[BUTTON_CONFIG_LENGTH * i + 1]
                shift = config[BUTTON_CONFIG_LENGTH * i + 2]
                alt = config[BUTTON_CONFIG_LENGTH * i + 3]
                key = config[BUTTON_CONFIG_LENGTH * i + 4]
                self.key_combination_edits[i].set_combination(key, ctrl, shift, alt)

    def get_config(self):
        config = bytearray()
        for i in range(BUTTONS_COUNT):
            is_text = self.is_text_checkboxes[i].isChecked()
            config.append(is_text)

            if is_text:
                config.append(0)
                config.append(0)
                config.append(0)
                config += self.text_fields[i].text().ljust(BUTTON_TEXT_LENGTH, "\0").encode("latin-1")
            else:
                key, ctrl, shift, alt = self.key_combination_edits[i].combination()
                config.append(ctrl)
                config.append(shift)
                config.append(alt)

                text = bytearray(BUTTON_TEXT_LENGTH)
                text[0] = key
                config += text

        return config

    def open(self):
        filename = QtGui.QFileDialog.getOpenFileName(self)
        if filename:
            with open(filename, 'r') as file:
                self.set_config(file.readline().encode("latin-1"))
            print("Loaded config from: " + filename)

    def save(self):
        filename = QtGui.QFileDialog.getSaveFileName(self)
        if filename:
            with open(filename, 'w') as file:
                file.write(self.get_config().decode("latin-1"))
            print("Saved config to: " + filename)

    def load(self):
        config = self.board.load()

        if not config:
            # If we don't receive config - show user appropriate message
            ret = self.show_reconnect_dialog()
            if ret == QtGui.QMessageBox.Retry:
                self.load()
            return
        self.set_config(config)
        print("Loaded config from board")

    def upload(self):
        config = self.get_config()
        print(config)

        ret = self.board.upload(config)
        if not ret:
            ret = self.show_reconnect_dialog()
            if ret == QtGui.QMessageBox.Retry:
                self.upload()
            return
        print("Config uploaded to board")

        self.load()
        self.show_upload_success_message()


if __name__ == "__main__":
    app = QtGui.QApplication(sys.argv)
    configurator = Configurator()
    configurator.show()
    sys.exit(app.exec_())
